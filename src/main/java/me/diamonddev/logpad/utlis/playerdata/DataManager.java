package me.diamonddev.logpad.utlis.playerdata;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import me.diamonddev.logpad.Plugin;
import me.diamonddev.logpad.utlis.server.Log;

public class DataManager {
	public static File datafolder = Plugin.gi().getDataFolder();

	public static PlayerData getPlayerData(UUID uuid) {
		File userdataFolder = new File(datafolder, "usetdata");
		if (!userdataFolder.exists())
			userdataFolder.mkdir();
		String filePath = "usetdata/" + uuid.toString() + ".yml";
		File file = new File(datafolder, filePath);
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				Log.warning(null, "Could not load " + Bukkit.getPlayer(uuid.toString()).getName() + "'s userdata.");
				e.printStackTrace();
			}
		}
		return new PlayerData(file, uuid);
	}

	public static List<PlayerData> getAllOnlinePlayerData() {
		List<PlayerData> datas = new ArrayList<PlayerData>();
		for (Player p : Bukkit.getOnlinePlayers()) {
			datas.add(getPlayerData(p.getUniqueId()));
		}
		return datas;
	}

	public static List<PlayerData> getAllPlayerData() {
		List<PlayerData> datas = new ArrayList<PlayerData>();
		File userdataFolder = new File(datafolder, "usetdata");
		File[] dataFiles = userdataFolder.listFiles();
		for (int i = 0; i < dataFiles.length; i++) {
			datas.add(getPlayerData(UUID.fromString(dataFiles[i].getName().replaceAll(".yml", ""))));
		}
		return datas;
	}
}
