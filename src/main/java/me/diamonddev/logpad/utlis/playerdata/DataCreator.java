package me.diamonddev.logpad.utlis.playerdata;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class DataCreator implements Listener {

	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		final Player p = e.getPlayer();
		PlayerData data = DataManager.getPlayerData(p.getUniqueId());
		data.getData().set("Username", p.getName());
		data.saveData();
		data.reloadData();
	}

	@EventHandler
	public void onLeave(PlayerQuitEvent e) {
		Player p = e.getPlayer();
		PlayerData data = DataManager.getPlayerData(p.getUniqueId());
		data.getData().set("Username", p.getName());
		data.getData().set("logOffTime", System.currentTimeMillis());
		data.getData().set("Location.world", p.getWorld().getName());
		data.getData().set("Location.x", p.getLocation().getX());
		data.getData().set("Location.y", p.getLocation().getY());
		data.getData().set("Location.z", p.getLocation().getZ());
		data.saveData();

	}
}
