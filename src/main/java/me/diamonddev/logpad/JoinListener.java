package me.diamonddev.logpad;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.scheduler.BukkitRunnable;

import me.diamonddev.logpad.signin.SignIn;
import me.diamonddev.logpad.utlis.playerdata.DataManager;
import me.diamonddev.logpad.utlis.playerdata.PlayerData;

public class JoinListener implements Listener {

	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		final Player p = event.getPlayer();
		PlayerData data = DataManager.getPlayerData(p.getUniqueId());
		if (!data.getData().contains("logOffTime")) {
			p.sendMessage(ChatColor.RED + "" + ChatColor.BOLD + "(!)" + ChatColor.YELLOW
					+ " If you want to protect your account do \"/register\"!");
		} else if (DataManager.getPlayerData(p.getUniqueId()).isPinSet()) {
			new BukkitRunnable() {

				public void run() {
					SignIn.signIn(p);
				}
			}.runTaskLater(Plugin.gi(), 25L);
		} else {
			return;
		}
	}
}
