package me.diamonddev.logpad.signin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class SignIn {
	public static List<UUID> signingIn = new ArrayList<UUID>();
	HashMap<Player, Inventory> signingInInvs = new HashMap<Player, Inventory>();

	public static Inventory signIn(Player p) {
		if (SignIn.signingIn.contains(p.getUniqueId()))
			signingIn.remove(p.getUniqueId());

		Inventory inv = Bukkit.createInventory(p, 54,
				ChatColor.GREEN + "Login" + ChatColor.DARK_GRAY + "" + ChatColor.BOLD + " > " + ChatColor.YELLOW);

		// short[] Colors = { 1, 2, 3, 6, 9, 10, 11 };
		short[] Colors = { 15 };
		Integer[] numPadArray = { 12, 13, 14, 21, 22, 23, 30, 31, 32 };
		List<Integer> numPad = Arrays.asList(numPadArray);

		ItemStack num0 = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 7);
		ItemMeta numIM0 = num0.getItemMeta();
		numIM0.setDisplayName(ChatColor.GRAY + "0");
		num0.setItemMeta(numIM0);
		ItemStack num1 = new ItemStack(Material.STAINED_GLASS_PANE, 1, Colors[new Random().nextInt(Colors.length)]);
		ItemMeta numIM1 = num1.getItemMeta();
		numIM1.setDisplayName(ChatColor.GRAY + "1");
		num1.setItemMeta(numIM1);
		ItemStack num2 = new ItemStack(Material.STAINED_GLASS_PANE, 2, Colors[new Random().nextInt(Colors.length)]);
		ItemMeta numIM2 = num2.getItemMeta();
		numIM2.setDisplayName(ChatColor.GRAY + "2");
		num2.setItemMeta(numIM2);
		ItemStack num3 = new ItemStack(Material.STAINED_GLASS_PANE, 3, Colors[new Random().nextInt(Colors.length)]);
		ItemMeta numIM3 = num3.getItemMeta();
		numIM3.setDisplayName(ChatColor.GRAY + "3");
		num3.setItemMeta(numIM3);
		ItemStack num4 = new ItemStack(Material.STAINED_GLASS_PANE, 4, Colors[new Random().nextInt(Colors.length)]);
		ItemMeta numIM4 = num4.getItemMeta();
		numIM4.setDisplayName(ChatColor.GRAY + "4");
		num4.setItemMeta(numIM4);
		ItemStack num5 = new ItemStack(Material.STAINED_GLASS_PANE, 5, Colors[new Random().nextInt(Colors.length)]);
		ItemMeta numIM5 = num5.getItemMeta();
		numIM5.setDisplayName(ChatColor.GRAY + "5");
		num5.setItemMeta(numIM5);
		ItemStack num6 = new ItemStack(Material.STAINED_GLASS_PANE, 6, Colors[new Random().nextInt(Colors.length)]);
		ItemMeta numIM6 = num6.getItemMeta();
		numIM6.setDisplayName(ChatColor.GRAY + "6");
		num6.setItemMeta(numIM6);
		ItemStack num7 = new ItemStack(Material.STAINED_GLASS_PANE, 7, Colors[new Random().nextInt(Colors.length)]);
		ItemMeta numIM7 = num7.getItemMeta();
		numIM7.setDisplayName(ChatColor.GRAY + "7");
		num7.setItemMeta(numIM7);
		ItemStack num8 = new ItemStack(Material.STAINED_GLASS_PANE, 8, Colors[new Random().nextInt(Colors.length)]);
		ItemMeta numIM8 = num8.getItemMeta();
		numIM8.setDisplayName(ChatColor.GRAY + "8");
		num8.setItemMeta(numIM8);
		ItemStack num9 = new ItemStack(Material.STAINED_GLASS_PANE, 9, Colors[new Random().nextInt(Colors.length)]);
		ItemMeta numIM9 = num9.getItemMeta();
		numIM9.setDisplayName(ChatColor.GRAY + "9");
		num9.setItemMeta(numIM9);

		ItemStack[] numPadItemsArray = { num1, num2, num3, num4, num5, num6, num7, num8, num9 };
		List<ItemStack> numPadItems = Arrays.asList(numPadItemsArray);
		List<Integer> usedPadItem = new ArrayList<Integer>();

		for (Integer numPadSlot : numPad) {
			int RandomItem = getNumber(usedPadItem, numPadItems);
			ItemStack item = numPadItems.get(RandomItem).clone();
			inv.setItem(numPadSlot, item);
			usedPadItem.add((Integer) RandomItem);
		}
		inv.setItem(40, num0);

		ItemStack yellow = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 4);
		ItemMeta blankIM = yellow.getItemMeta();
		blankIM.setDisplayName(ChatColor.RESET + "");
		yellow.setItemMeta(blankIM);
		ItemStack white = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 0);
		white.setItemMeta(blankIM);
		ItemStack black = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 13);
		black.setItemMeta(blankIM);
		ItemStack red = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 14);
		ItemMeta redIM = yellow.getItemMeta();
		redIM.setDisplayName(ChatColor.RED + "Cancel");
		red.setItemMeta(redIM);
		ItemStack green = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 5);
		ItemMeta greenIM = yellow.getItemMeta();
		greenIM.setDisplayName(ChatColor.GREEN + "Login");
		green.setItemMeta(greenIM);
		ItemStack back = new ItemStack(Material.IRON_FENCE, 1);
		ItemMeta backIM = yellow.getItemMeta();
		backIM.setDisplayName(ChatColor.YELLOW + "Back");
		back.setItemMeta(backIM);
		int[] yellowSlots = { 0, 8, 10, 16, 18, 26, 28, 34, 36, 44, 46, 52 };
		int[] whiteSlots = { 1, 7, 9, 17, 19, 25, 27, 35, 37, 43, 45, 53 };
		int[] blackSlots = { 2, 3, 4, 5, 6, 11, 20, 24, 29, 33, 38, 42, 47, 48, 49, 50, 51 };
		int redSlot = 41;
		int greenSlot = 39;
		int backSlot = 15;

		for (int i = 0; i < yellowSlots.length; i++)
			inv.setItem(yellowSlots[i], yellow);

		for (int i = 0; i < whiteSlots.length; i++)
			inv.setItem(whiteSlots[i], white);

		for (int i = 0; i < blackSlots.length; i++)
			inv.setItem(blackSlots[i], black);

		inv.setItem(redSlot, red);

		inv.setItem(greenSlot, green);

		inv.setItem(backSlot, back);
		signingIn.add(p.getUniqueId());
		p.openInventory(inv);
		return inv;
	}

	private static int getNumber(List<Integer> used, List<?> from) {
		int RandomItem = new Random().nextInt(from.size());
		if (used.contains(RandomItem)) {
			RandomItem = getNumber(used, from);
		}
		return RandomItem;
	}
}
